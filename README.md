# POLAR WP 1.4 Pilot

WP 1.4 pilot container registry project for the [POLAR MII Use Case](https://www.medizininformatik-initiative.de/de/POLAR). This project solely exists for the private GitLab Container Registry and will be removed in the future, when the images become public or are moved to a dedicated location.
